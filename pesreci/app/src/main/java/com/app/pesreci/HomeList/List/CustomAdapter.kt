package com.app.pesreci.HomeList.List

import android.content.res.TypedArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import com.app.pesreci.MainActivity


class CustomAdapter(
    private val dataSet: ArrayList<ListEntry>,
    val list: Folder,
    val parent: MainActivity,
    var visibility: Int = View.INVISIBLE
) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    val Items = arrayListOf<ViewHolder>()


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val button: Button
        val button_down: ImageButton
        val button_del: ImageButton

        init {
            button = view.findViewById(com.app.pesreci.R.id.button_list)
            button_down = view.findViewById(com.app.pesreci.R.id.down_button)
            button_del = view.findViewById(com.app.pesreci.R.id.del_button)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(com.app.pesreci.R.layout.list_item, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.button.text = dataSet[position].name
        Items.add(viewHolder)
        if (!dataSet[position].isRecipe()) {
            val ta: TypedArray = parent.obtainStyledAttributes(
                null,
                com.app.pesreci.R.styleable.MyCustomView,
                com.app.pesreci.R.attr.customViewStyle,
                0
            )
            viewHolder.button.setBackgroundColor(
                ta.getColor(
                    com.app.pesreci.R.styleable.MyCustomView_colorSecondary,
                    0
                )
            )
            viewHolder.button.setTextColor(
                ta.getColor(
                    com.app.pesreci.R.styleable.MyCustomView_colorOnSecondary,
                    0
                )
            )
            ta.recycle()
        }

        viewHolder.button.setTextSize(parent.design.getTextSize("n"))

        viewHolder.button.setOnClickListener {
            list.show_Entry(position)
        }
        viewHolder.button_del.setOnClickListener {
            parent.removeItem(position)
        }
        viewHolder.button_down.setOnClickListener {
            parent.currentFolder.switchItem(position)
        }

        visibilityRefresh()
    }

    override fun getItemCount() = dataSet.size

    fun visibilityRefresh() {
        for (i in Items) {
            i.button_del.visibility = visibility
            i.button_down.visibility = visibility
        }
    }

}
