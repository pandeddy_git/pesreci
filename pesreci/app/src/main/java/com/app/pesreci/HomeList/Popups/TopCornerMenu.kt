package com.app.pesreci.HomeList.Popups

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.app.pesreci.HomeList.List.CustomAdapter
import com.app.pesreci.HomeList.List.Recipe
import com.app.pesreci.MainActivity
import com.app.pesreci.R
import com.app.pesreci.help.Design


class TopCornerMenu(val parent: MainActivity) : DialogFragment() {

    lateinit var rootView: View
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        //rootView = inflater.inflate(R.layout.popup_topcornermenu, container, false)


        val window = dialog!!.window!!
        val p = window.attributes
        window.setGravity(Gravity.END or Gravity.TOP)
        p.softInputMode = WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
        p.x = 40
        p.y = 55
        window.attributes = p

        funcionality()
        return rootView
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dia = activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater;

            rootView = inflater.inflate(R.layout.popup_topcornermenu, null)

            builder.setView(rootView)


            builder.create()


        } ?: throw IllegalStateException("Activity cannot be null")

        return dia
    }

    fun readDesignfromIntent(i: Intent): Design {
        val design = Design()
        design.setTextSize(i.getFloatExtra("text", 22F))
        return design
    }


    fun funcionality() {

        rootView.findViewById<TextView>(R.id.button_tc_edit).textSize =
            parent.design.getTextSize("n")
        rootView.findViewById<TextView>(R.id.button_tc_addFolder).textSize =
            parent.design.getTextSize("n")
        rootView.findViewById<TextView>(R.id.button_tc_Copy).textSize =
            parent.design.getTextSize("n")
        rootView.findViewById<TextView>(R.id.button_tc_Paste).textSize =
            parent.design.getTextSize("n")

        rootView.findViewById<TextView>(R.id.button_tc_edit).setOnClickListener {
            (parent.currentFolder.recview.adapter as CustomAdapter).visibility = View.VISIBLE
            (parent.currentFolder.recview.adapter as CustomAdapter).visibilityRefresh()
            this.dismiss()
        }

        rootView.findViewById<TextView>(R.id.button_tc_addFolder).setOnClickListener {
            parent.addFolder()
            this.dismiss()
        }

        rootView.findViewById<TextView>(R.id.button_tc_Copy).setOnClickListener {
            parent.copy()
            this.dismiss()
        }

        rootView.findViewById<TextView>(R.id.button_tc_Paste).setOnClickListener {
            if (parent.currentRecipe == null) {
                parent.currentFolder.paste()
            }

            this.dismiss()
        }

    }
}


