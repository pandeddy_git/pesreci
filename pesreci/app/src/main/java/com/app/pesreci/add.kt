package com.app.pesreci

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import com.app.pesreci.HomeList.Home
import com.app.pesreci.HomeList.List.Recipe

class add(val home: Home) : Fragment() {
    lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)
        rootView = inflater.inflate(R.layout.fragment_add, container, false)
        rootView.findViewById<Button>(R.id.add_button).setOnClickListener { add() }
        rootView.findViewById<Button>(R.id.add_button)
            .setTextSize(home.parent.design.getTextSize("n"))
        rootView.findViewById<EditText>(R.id.add_name)
            .setTextSize(home.parent.design.getTextSize("n"))
        rootView.findViewById<EditText>(R.id.add_ingredients)
            .setTextSize(home.parent.design.getTextSize("n"))
        rootView.findViewById<EditText>(R.id.add_description)
            .setTextSize(home.parent.design.getTextSize("n"))
        return rootView
    }

    fun add() {
        val name = rootView.findViewById<EditText>(R.id.add_name).text.toString()
        val ingredients = rootView.findViewById<EditText>(R.id.add_ingredients).text.toString()
        val description = rootView.findViewById<EditText>(R.id.add_description).text.toString()

        val recipe = Recipe(name, ArrayList(), description)
        recipe.insert_ingredients(ingredients)

        home.parent.currentFolder.addItem(recipe)
    }


}