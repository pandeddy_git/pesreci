package com.app.pesreci.HomeList.Popups

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Display
import android.widget.Button
import android.widget.TextView
import androidx.core.hardware.display.DisplayManagerCompat
import com.app.pesreci.HomeList.Home
import com.app.pesreci.R
import com.app.pesreci.help.Design

class del_popup() : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.del_popup)

        val defaultDisplay =
            DisplayManagerCompat.getInstance(this).getDisplay(Display.DEFAULT_DISPLAY)
        val displayContext = createDisplayContext(defaultDisplay!!)
        val w = displayContext.resources.displayMetrics.widthPixels
        val h = displayContext.resources.displayMetrics.heightPixels
        window.setLayout((w * .9).toInt(), (h * .25).toInt())

        val int = Intent()
        findViewById<Button>(R.id.del_yes).setOnClickListener {
            val n = intent.getIntExtra("n", -1)
            println(n)
            int.putExtra("rn", n)
            setResult(RESULT_OK, int)
            finish()
        }

        findViewById<Button>(R.id.del_no).setOnClickListener {
            int.putExtra("rn", -1)
            setResult(RESULT_OK, int)
            finish()
        }

        val design = readDesignfromIntent(intent)
        findViewById<Button>(R.id.del_no).setTextSize(design.getTextSize("s"))
        findViewById<Button>(R.id.del_yes).setTextSize(design.getTextSize("s"))
        findViewById<TextView>(R.id.del_text).setTextSize(design.getTextSize("s"))

    }

    fun readDesignfromIntent(i: Intent): Design {
        val design = Design()
        design.setTextSize(i.getFloatExtra("text", 22F))
        return design
    }
}

