package com.app.pesreci.help

class Design {


    val standardText: Float = 26f
    var textsize: Float = standardText
    val factorsmall = .8
    val factorbig = 1.5
    var factor = 1.0

    fun setTextSize(s: Float) {
        textsize = s
    }

    fun multiplyStandardSize(f: Double) {
        factor = f
    }

    fun getTextSize(s: String): Float {
        when (s) {
            "s" -> return (textsize * factorsmall * factor).toFloat()
            "n" -> return (textsize * factor).toFloat()
            "b" -> return (textsize * factorbig * factor).toFloat()
        }

        return 0F
    }


}