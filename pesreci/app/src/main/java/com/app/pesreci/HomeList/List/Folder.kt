package com.app.pesreci.HomeList.List

import android.security.identity.CredentialDataResult.Entries
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.pesreci.HomeList.Home
import com.app.pesreci.HomeList.Recipe_view
import com.app.pesreci.MainActivity
import com.app.pesreci.R
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlSerializer

class Folder(override var name: String, val parent: MainActivity, val home: Home) : ListEntry() {
    val entries = arrayListOf<ListEntry>()
    lateinit var recview: RecyclerView

    override fun isRecipe(): Boolean {
        return false
    }

    fun addEntry(e: ListEntry) {
        entries.add(e)
    }

    fun removeEntry(n: Int) {
        entries.removeAt(n)
    }

    fun removeRecipe(n: Int) {
        removeEntry(n)
        home.save_data()
        parent.repFrag(home)
        show_list(home.rootView, View.VISIBLE)
    }

    fun addItem(e: ListEntry) {
        addEntry(e)
        home.save_data()
        parent.repFrag(home)
        show_list(home.rootView)
    }


    fun switchItem(n: Int) {
        if (n + 1 < entries.size) {
            val tmp = entries.get(n)
            entries.set(n, entries.get(n + 1))
            entries.set(n + 1, tmp)
        }
        show_list(home.rootView, View.VISIBLE)
    }

    fun show_list(view: View, visibility: Int = View.INVISIBLE) {
        parent.currentFolder = this
        home.rootView.findViewById<TextView>(R.id.text_currentFolder).setText(name)
        home.rootView.findViewById<TextView>(R.id.text_currentFolder)
            .setTextSize(parent.design.getTextSize("n"))
        recview = view.findViewById<View>(R.id.recview) as RecyclerView
        recview.layoutManager = LinearLayoutManager(view.context)
        recview.adapter = CustomAdapter(entries, this, parent, visibility)
    }

    fun show_Entry(n: Int) {
        if (n < entries.size) {
            if (entries[n].isRecipe()) {
                show_recipe(n)
            } else {
                (entries[n] as Folder).show_list(home.rootView)
            }
        } else {
            parent.addFolder()
        }

    }

    fun show_recipe(n: Int) {
        parent.currentRecipe = entries[n] as Recipe
        parent.repFrag(Recipe_view(entries[n] as Recipe, home))
    }


    override fun saveDataToXmlFile(serializer: XmlSerializer) {
        serializer.startTag(null, "Folder")
        serializer.startTag(null, "name")
        serializer.text(name)
        serializer.endTag(null, "name")
        for (e in entries) {
            e.saveDataToXmlFile(serializer)
        }
        serializer.endTag(null, "Folder")
    }

    override fun loadDataFromXmlFile(parser: XmlPullParser): ListEntry {
        var eventType = parser.next()
        if (eventType == XmlPullParser.START_TAG && parser.name == "name") {
            parser.next()
            name = parser.text
            parser.next()
        }
        eventType = parser.next()
        while (eventType != XmlPullParser.END_TAG || parser.name != "Folder") {
            if (eventType == XmlPullParser.START_TAG && parser.name == "recipe") {
                addEntry(Recipe("", arrayListOf(), "").loadDataFromXmlFile(parser))
            } else if (eventType == XmlPullParser.START_TAG && parser.name == "Folder") {
                addEntry(Folder("", parent, home).loadDataFromXmlFile(parser))
            }
            eventType = parser.next()
        }
        return this
    }

    override fun copy(): ListEntry {
        val Copy = Folder(name, parent, home)
        for (e in entries) {
            Copy.addItem(e.copy())
        }
        return Copy
    }

    fun paste() {
        if (parent.copy != null) {
            addItem(parent.copy!!)
        }

    }
}