package com.app.pesreci

import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.view.Menu
import android.widget.ImageButton
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.app.pesreci.HomeList.Home
import com.app.pesreci.HomeList.List.Folder
import com.app.pesreci.HomeList.List.ListEntry
import com.app.pesreci.HomeList.List.Recipe
import com.app.pesreci.HomeList.Popups.TopCornerMenu
import com.app.pesreci.HomeList.Popups.del_popup
import com.app.pesreci.HomeList.Popups.folder_popup
import com.app.pesreci.databinding.ActivityMainBinding
import com.app.pesreci.help.Design


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var home: Home
    lateinit var settings: AppSettings
    lateinit var design: Design
    lateinit var currentFolder: Folder
    var currentRecipe: Recipe? = null
    var copy: ListEntry? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        home = Home(getApplicationInfo().dataDir, this)
        design = Design()
        settings = AppSettings(this)


        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        repFrag(home)

        binding.nav.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    currentRecipe = null
                    repFrag(home)
                    home.root.show_list(home.rootView)
                }

                R.id.settings -> repFrag(settings)
                R.id.add -> repFrag(add(home))
                else -> {}
            }
            true
        }

        findViewById<TextView>(R.id.PesReci).setTextSize(
            TypedValue.COMPLEX_UNIT_SP,
            home.parent.design.getTextSize("b")
        )

        findViewById<ImageButton>(R.id.button_tc).setOnClickListener {
            val tc = TopCornerMenu(this)
            tc.show(supportFragmentManager, "123")
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.navfolder_corner, menu)
        return true
    }

    fun repFrag(fragment: Fragment) {
        val fragtran = supportFragmentManager.beginTransaction()
        fragtran.replace(R.id.container, fragment)
        fragtran.commit()

        if (this::currentFolder.isInitialized) {
            currentFolder.show_list(home.rootView)
        }
    }

    fun removeItem(n: Int) {
        val int = Intent(this, del_popup::class.java)
        int.putExtra("n", n)
        int.putExtra("text", design.getTextSize("n"))

        result_remove.launch(int)
    }

    fun addFolder() {
        val int = Intent(this, folder_popup::class.java)
        int.putExtra("text", design.getTextSize("n"))

        result_addFolder.launch(int)
    }


    private val result_remove = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == RESULT_OK) {
            val rn = it.data?.getIntExtra("rn", -1)!!
            if (rn >= 0) {
                currentFolder.removeRecipe(rn)
            }
        }
    }

    private val result_addFolder = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (it.resultCode == RESULT_OK) {
            val rn = it.data?.getStringExtra("i")!!
            if (rn != "") {
                currentFolder.addItem(Folder(rn, this, home))
            }
        }
    }

    fun currentInitialized(): Boolean {
        return this::currentFolder.isInitialized
    }

    fun copy() {
        if (currentRecipe == null) {
            if (currentFolder != home.root) {
                copy = currentFolder.copy()
            }

        } else {
            copy = currentRecipe!!.copy()
        }

    }


}

