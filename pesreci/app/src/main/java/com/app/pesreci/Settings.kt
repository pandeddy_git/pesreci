package com.app.pesreci

import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserFactory
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception


class AppSettings(val parent: MainActivity) : Fragment() {
    lateinit var rootView: View
    val file = File("${parent.getApplicationInfo().dataDir}/files/settings.xml")

    val dic: HashMap<String, String> = hashMapOf(
        "textSizeFactor" to "1.0",
    )

    init {
        try {
            File("${parent.getApplicationInfo().dataDir}/files").mkdirs()
        } catch (_: Exception) {

        }
        if (!file.exists()) {
            file.createNewFile()
            savetoXML()
        }
        loadSettings()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        rootView = inflater.inflate(R.layout.fragment_settings, container, false)

        refresh()
        rootView.findViewById<Button>(R.id.button_save).setOnClickListener {
            parent.design.multiplyStandardSize(
                rootView.findViewById<EditText>(R.id.input_textSize).text.toString().toDouble()
            )
            refresh()
            saveSettings()
        }
        return rootView
    }

    private fun loadSettings() {
        loadfromXML()

        parent.design.multiplyStandardSize(dic["textSizeFactor"]!!.toDouble())
    }

    private fun saveSettings() {
        dic["textSizeFactor"] = parent.design.factor.toString()
        savetoXML()
    }


    fun refresh() {

        val editText = rootView.findViewById<EditText>(R.id.input_textSize)
        rootView.findViewById<TextView>(R.id.text_textSize).textSize =
            parent.design.getTextSize("n")
        editText.textSize = parent.design.getTextSize("n")
        editText.text = Editable.Factory.getInstance().newEditable(parent.design.factor.toString())
        rootView.findViewById<Button>(R.id.button_save).textSize = parent.design.getTextSize("n")
    }

    fun savetoXML() {
        val serializer = XmlPullParserFactory.newInstance().newSerializer()
        serializer.setOutput(FileOutputStream(file), "UTF-8")
        serializer.startDocument(null, true)
        serializer.startTag(null, "settings")
        for (key in dic.keys) {
            serializer.startTag(null, key)
            serializer.text(dic[key])
            serializer.endTag(null, key)
        }
        serializer.endTag(null, "settings")
        serializer.endDocument()
    }

    fun loadfromXML() {
        val factory = XmlPullParserFactory.newInstance()
        factory.isNamespaceAware = true
        val parser = factory.newPullParser()
        parser.setInput(file.inputStream(), "UTF-8")
        var event = parser.eventType

        while (event != XmlPullParser.END_DOCUMENT) {
            if (event == XmlPullParser.START_TAG && parser.name in dic.keys) {
                val key = parser.name
                parser.next()
                dic[key] = parser.text
                parser.next()
            }
            event = parser.next()
        }

    }


}