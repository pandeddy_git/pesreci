package com.app.pesreci.HomeList

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.app.pesreci.R
import com.app.pesreci.HomeList.List.Recipe

class Recipe_view(val recipe: Recipe, val home: Home) : Fragment() {

    lateinit var rootView: View

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        rootView = inflater.inflate(R.layout.recipe_view, container, false)

        rootView.findViewById<TextView>(R.id.recipe_name).text = recipe.name
        rootView.findViewById<TextView>(R.id.recipe_name)
            .setTextSize(TypedValue.COMPLEX_UNIT_SP, home.parent.design.getTextSize("n"))
        rootView.findViewById<TextView>(R.id.recipe_ingredients).text =
            recipe.print_ingredients()
        rootView.findViewById<TextView>(R.id.recipe_ingredients)
            .setTextSize(TypedValue.COMPLEX_UNIT_SP, home.parent.design.getTextSize("n"))
        rootView.findViewById<TextView>(R.id.recipe_description).text = recipe.description
        rootView.findViewById<TextView>(R.id.recipe_description)
            .setTextSize(TypedValue.COMPLEX_UNIT_SP, home.parent.design.getTextSize("n"))

        return rootView
    }

}