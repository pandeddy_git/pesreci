package com.app.pesreci.HomeList.List

import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlSerializer

open class ListEntry {

    open lateinit var name: String

    open fun isRecipe(): Boolean {
        return false
    }

    /* open fun getContent(): ArrayList<String>{
         return arrayListOf<String>()
     }
 */

    open fun saveDataToXmlFile(serializer: XmlSerializer) {

    }

    open fun loadDataFromXmlFile(parser: XmlPullParser): ListEntry {
        return ListEntry()
    }

    open fun copy(): ListEntry {
        return this
    }


}