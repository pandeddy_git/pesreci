package com.app.pesreci.help

import com.app.pesreci.HomeList.List.Folder
import org.xmlpull.v1.XmlPullParserFactory
import java.io.File
import java.io.FileOutputStream

class help {
    // Funktion zum Speichern von Daten in einer XML-Datei
    fun saveDataToXmlFile(file: File, data: Folder) {
        // XmlSerializer und XmlPullParserFactory initialisieren

        val serializer = XmlPullParserFactory.newInstance().newSerializer()
        serializer.setOutput(FileOutputStream(file), "UTF-8")
        serializer.startDocument(null, true)
        serializer.startTag(null, "recipes")
        // Daten als XML-Elemente speichern
        data.saveDataToXmlFile(serializer)
        serializer.endTag(null, "recipes")
        serializer.endDocument()
    }

    // Funktion zum Lesen von Daten aus einer XML-Datei
    fun loadDataFromXmlFile(file: File, folder: Folder): Folder {

        // XmlPullParserFactory initialisieren
        val factory = XmlPullParserFactory.newInstance()
        factory.isNamespaceAware = true
        val parser = factory.newPullParser()

        // XML-Datei einlesen
        parser.setInput(file.inputStream(), "UTF-8")

        // Daten aus XML-Datei auslesen
        parser.next()
        parser.next()
        folder.loadDataFromXmlFile(parser)
        return folder
    }
}

data class Ingredient(var name: String, var amount: String)