package com.app.pesreci.HomeList

import com.app.pesreci.HomeList.List.Folder
import com.app.pesreci.help.Ingredient
import com.app.pesreci.HomeList.List.Recipe

fun recipes_examples(folder: Folder) {

    folder.addEntry(
        Recipe(
            "Example",
            arrayListOf<Ingredient>(
                Ingredient("Eggs", "2"),
                Ingredient("Milk", "200ml"),
            ),
            "Here is space for the description"
        )
    )


    val f = Folder("ExampleFolder", folder.parent, folder.home)

    folder.addEntry(f)

}