package com.app.pesreci.HomeList.List

import com.app.pesreci.help.Ingredient
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlSerializer

data class Recipe(
    override var name: String,
    var ingredients: ArrayList<Ingredient>,
    var description: String
) : ListEntry() {

    override fun isRecipe(): Boolean {
        return true
    }

    fun print_ingredients(): String {
        var string = ""

        for (i in ingredients) {
            string = string.plus("${i.amount}  ${i.name} \n")
        }

        return string
    }

    fun insert_ingredients(string: String) {
        val input = string.split("\n").filter { it.isNotEmpty() }.toList()
        val output = ArrayList<Ingredient>()

        for (i in input) {
            val ing = i.split(" ").filter { it.isNotEmpty() }.toList()

            when (ing.size) {
                0 -> break
                1 -> output.add(Ingredient(ing[0], ""))
                2 -> output.add(Ingredient(ing[1], ing[0]))
                else -> output.add(Ingredient(ing.drop(1).joinToString(separator = " "), ing[0]))
            }
            println("${ing}")
        }
        ingredients = output
    }

    override fun saveDataToXmlFile(serializer: XmlSerializer) {
        serializer.startTag(null, "recipe")
        serializer.startTag(null, "name")
        serializer.text(name)
        serializer.endTag(null, "name")
        serializer.startTag(null, "ingredients")
        for (ing in ingredients) {
            serializer.startTag(null, "ingredient_name")
            serializer.text(ing.name)
            serializer.endTag(null, "ingredient_name")
            serializer.startTag(null, "amount")
            serializer.text(ing.amount)
            serializer.endTag(null, "amount")
        }
        serializer.endTag(null, "ingredients")
        serializer.startTag(null, "description")
        serializer.text(description)
        serializer.endTag(null, "description")
        serializer.endTag(null, "recipe")
    }

    override fun loadDataFromXmlFile(parser: XmlPullParser): ListEntry {
        var eventType = parser.next()
        while (eventType != XmlPullParser.END_TAG || parser.name != "recipe") {
            if (eventType == XmlPullParser.START_TAG && parser.name == "name") {
                parser.next()
                if (parser.text == null) name = ""
                else {
                    name = parser.text
                    parser.next()
                }
            }
            eventType = parser.next()
            if (eventType == XmlPullParser.START_TAG && parser.name == "ingredients") {
                eventType = parser.next()
                while (eventType != XmlPullParser.END_TAG || parser.name != "ingredients") {
                    var ingName = ""
                    var amount = ""
                    if (eventType == XmlPullParser.START_TAG && parser.name == "ingredient_name") {
                        parser.next()
                        if (parser.text == null) ingName = ""
                        else {
                            ingName = parser.text
                            parser.next()
                        }
                    }
                    eventType = parser.next()

                    if (eventType == XmlPullParser.START_TAG && parser.name == "amount") {
                        parser.next()
                        if (parser.text == null) amount = ""
                        else {
                            amount = parser.text
                            parser.next()
                        }

                    }
                    eventType = parser.next()
                    ingredients.add(Ingredient(ingName, amount))
                }
                eventType = parser.next()
            }

            if (eventType == XmlPullParser.START_TAG && parser.name == "description") {
                parser.next()
                if (parser.text == null) description = ""
                else {
                    description = parser.text
                    parser.next()
                }
            }
            eventType = parser.next()
        }

        return this
    }

    override fun copy(): ListEntry {
        val Copy = arrayListOf<Ingredient>()
        for (i in ingredients) {
            Copy.add(Ingredient(i.name, i.amount))
        }
        return Recipe(name, Copy, description)
    }
}
