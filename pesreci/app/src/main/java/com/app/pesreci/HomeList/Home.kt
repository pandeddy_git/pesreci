package com.app.pesreci.HomeList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.pesreci.HomeList.List.Folder
import com.app.pesreci.help.help
import com.app.pesreci.MainActivity
import com.app.pesreci.R
import java.io.File
import java.lang.Exception


/**
 * A simple [Fragment] subclass.
 * Use the [Home.newInstance] factory method to
 * create an instance of this fragment.
 */
class Home(path: String, val parent: MainActivity) : Fragment() {

    val dir = path
    lateinit var rootView: View
    var root = Folder("Home", parent, this)


    init {
        val file = File("${dir}/files/recipes.xml")
        try {
            File("${dir}/files").mkdirs()
        } catch (_: Exception) {

        }
        if (!file.exists()) {
            file.createNewFile()
            save_examples()
        } else {
            root = help().loadDataFromXmlFile(file, root)
        }
        root.name = "Home"
        save_data()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreate(savedInstanceState)

        rootView = inflater.inflate(R.layout.fragment_home, container, false)

        if (!parent.currentInitialized()) {
            parent.currentFolder = root
        }
        parent.currentFolder.show_list(rootView)

        return rootView
    }

    fun save_examples() {
        recipes_examples(root)
        save_data()
    }

    fun save_data() {
        val file = File("${dir}/files/recipes.xml")
        help().saveDataToXmlFile(file, root)
    }

    /*fun addRecipe(recipe: Recipe) {
        root.addEntry(recipe)
        save_data()
        parent.repFrag(this)
    }*/
}


