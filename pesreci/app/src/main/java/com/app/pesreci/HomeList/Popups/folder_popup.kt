package com.app.pesreci.HomeList.Popups

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.Display
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.core.hardware.display.DisplayManagerCompat
import com.app.pesreci.HomeList.Home
import com.app.pesreci.R
import com.app.pesreci.help.Design

class folder_popup() : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.folder_popup)


        val defaultDisplay =
            DisplayManagerCompat.getInstance(this).getDisplay(Display.DEFAULT_DISPLAY)
        val displayContext = createDisplayContext(defaultDisplay!!)
        val w = displayContext.resources.displayMetrics.widthPixels
        val h = displayContext.resources.displayMetrics.heightPixels
        window.setLayout((w * .9).toInt(), (h * .25).toInt())

        val int = Intent()
        findViewById<Button>(R.id.addFolder_button).setOnClickListener {
            val input = findViewById<EditText>(R.id.addFolder_text).text.toString()
            int.putExtra("i", input)
            setResult(RESULT_OK, int)
            finish()
        }

        val design = readDesignfromIntent(intent)
        findViewById<Button>(R.id.addFolder_button).setTextSize(design.getTextSize("s"))
        findViewById<TextView>(R.id.addFolder_text).setTextSize(design.getTextSize("s"))

    }

    fun readDesignfromIntent(i: Intent): Design {
        val design = Design()
        design.setTextSize(i.getFloatExtra("text", 22F))
        return design
    }
}

